/**
 * Default settings for project
 *
 *
 */

export default {
  version: "1.0.4",
  checkUpdates: true,
  albumId: "08cbdbd1238d321", //It is the id of folder where we storing binaries for install
  downloadLink: "https://upfiler.sht-server.cf/#/download/?t=file&h=",
}
