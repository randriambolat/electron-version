# LET'S TO COLLABORATE

This project is an Open Source under MIT license. You can improve the project if you are:

## DEVELOPER

- Select (or create) a task/issue from our [task board](https://gitlab.com/go-apps-XnjRcS/vr-viewer-for-linux/electron-version/-/boards)
- Fork the Repository
- Create new branch from develop (we use [git flow](https://danielkummer.github.io/git-flow-cheatsheet/index.html) approach).
- For example: feature/task-name
- Push your improvements and create pull-request

## DESIGNER

If you are UI/UX designer please feel free to give us your ideas about how to improve VR-player.

We understand the importance of competent design and how this affects the end user. (I.e. for all of us). If you want to contribute, feel free to send your comments or layouts to the contacts listed on this https://sht-vr-player.cf/ page.


## SPONSOR
If you have a good experience with VR-player for linux and want to say thanks!
You can make a [donation](https://www.liqpay.ua/en/checkout/card/shtvrplayer).

We need your support to pay the costs of server services, ADVERTISING, etc.
